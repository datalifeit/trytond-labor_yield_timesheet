==============================
Labor Yield Timesheet Scenario
==============================

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> today = datetime.date.today()
    >>> tomorrow = today + relativedelta(days=1)
    >>> from decimal import Decimal


Install labor_yield_timesheet::

    >>> config = activate_modules('labor_yield_timesheet')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create employees::

    >>> Party = Model.get('party.party')
    >>> Employee = Model.get('company.employee')
    >>> party1 = Party(name='Empleado 1', code='00001')
    >>> party1.save()
    >>> party2 = Party(name='Empleado 2', code='00002')
    >>> party2.save()
    >>> party3 = Party(name='Empleado 2', code='00003')
    >>> party3.save()
    >>> employee1 = Employee(party=party1,company=company)
    >>> employee1.save()
    >>> employee2 = Employee(party=party2,company=company)
    >>> employee2.save()
    >>> employee3 = Employee(party=party3,company=company)
    >>> employee3.save()


Create work::

    >>> Work = Model.get('timesheet.work')
    >>> work = Work(name='Yield work 1', yield_available=True)
    >>> work.manual_yield_record = True
    >>> work.save()
    >>> config._context['work'] = work.id


Create yield allocations manually::

    >>> YAProcedure = Model.get('labor.yield.allocation.procedure')
    >>> proc = YAProcedure(name='Test LY Procedure', work=work)
    >>> proc.save()
    >>> YAlloc = Model.get('labor.yield.allocation')
    >>> y_alloc = YAlloc(date=today, employee=employee1, quantity=1000,
    ...     procedure=proc, offset_=1, work=work)
    >>> y_alloc.save()
    >>> y_alloc = YAlloc(date=today, employee=employee1, quantity=500,
    ...     procedure=proc, offset_=1, work=work)
    >>> y_alloc.save()
    >>> y_alloc = YAlloc(date=tomorrow, employee=employee1, quantity=900,
    ...     procedure=proc, offset_=1, work=work)
    >>> y_alloc.save()
    >>> y_alloc = YAlloc(date=today, employee=employee2, quantity=1100,
    ...     procedure=proc, offset_=1, work=work)
    >>> y_alloc.save()
    >>> y_alloc = YAlloc(date=today, employee=employee3, quantity=900,
    ...     work=work)
    >>> y_alloc.save()


Create timesheet lines::

    >>> create_timesheet = Wizard('labor.yield.create_timesheet', [])
    >>> create_timesheet.form.work = work
    >>> create_timesheet.form.start_date = today
    >>> create_timesheet.form.end_date = today + relativedelta(days=2)
    >>> create_timesheet.execute('create_') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ...


Configure unit price::

    >>> price1 = work.unit_prices.new()
    >>> price1.unit.category == work.uom_category
    True
    >>> price1.date = today
    >>> price1.unit_price = Decimal('0.05')
    >>> work.save()


Check timesheet lines::

    >>> create_timesheet = Wizard('labor.yield.create_timesheet', [])
    >>> create_timesheet.form.work = work
    >>> create_timesheet.form.start_date = today
    >>> create_timesheet.form.end_date = today + relativedelta(days=2)
    >>> create_timesheet.execute('create_') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ...


Configure employee cost price::

    >>> cost_price = employee1.cost_prices.new()
    >>> cost_price.date = today
    >>> cost_price.cost_price = Decimal('12.5')
    >>> employee1.save()
    >>> cost_price = employee2.cost_prices.new()
    >>> cost_price.date = today
    >>> cost_price.cost_price = Decimal('10')
    >>> employee2.save()
    >>> cost_price = employee3.cost_prices.new()
    >>> cost_price.date = today
    >>> cost_price.cost_price = Decimal('8')
    >>> employee3.save()


Try again::

    >>> create_timesheet = Wizard('labor.yield.create_timesheet', [])
    >>> create_timesheet.form.work = work
    >>> create_timesheet.form.start_date = today
    >>> create_timesheet.form.end_date = today + relativedelta(days=2)
    >>> create_timesheet.execute('create_')
    >>> Timesheetline = Model.get('timesheet.line')
    >>> lines = Timesheetline.find([])
    >>> len(lines)
    4


Check timesheet line data::

    >>> lines = Timesheetline.find([('employee', '=', employee1.id)])
    >>> len(lines)
    2
    >>> line, = [l for l in lines if l.date == today]
    >>> line.duration == datetime.timedelta(hours=6)
    True
    >>> line, = [l for l in lines if l.date == tomorrow]
    >>> line.duration == datetime.timedelta(hours=3.6)
    True
    >>> lines = Timesheetline.find([('employee', '=', employee2.id)])
    >>> len(lines)
    1
    >>> line, = lines
    >>> line.duration == datetime.timedelta(hours=5.5)
    True
    >>> line.date == today
    True
    >>> lines = Timesheetline.find([('employee', '=', employee3.id)])
    >>> len(lines)
    1
    >>> line, = lines
    >>> line.date == today
    True
    >>> line.duration == datetime.timedelta(hours=5.625)
    True


Delete yield allocations::

    >>> YAlloc.delete([y_alloc]) # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ...
    >>> create_timesheet = Wizard('labor.yield.delete_timesheet', [])
    >>> create_timesheet.form.work = work
    >>> create_timesheet.form.start_date = today
    >>> create_timesheet.form.end_date = today + relativedelta(days=2)
    >>> create_timesheet.execute('delete_')
    >>> not Timesheetline.find([])
    True
    >>> not YAlloc.find([('timesheet_line', '!=', None)])
    True
