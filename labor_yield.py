# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.exceptions import UserError
from trytond.i18n import gettext


class YieldAllocation(metaclass=PoolMeta):
    __name__ = 'labor.yield.allocation'

    timesheet_line = fields.Many2One('timesheet.line', 'Timesheet line')

    @classmethod
    def delete(cls, records):
        Lang = Pool().get('ir.lang')
        lang = Lang.get()
        for record in records:
            if record.timesheet_line:
                language = Transaction().language
                languages = Lang.search([('code', '=', language)])
                if not languages:
                    languages = Lang.search([('code', '=', 'en_US')])
                language, = languages
                _date = lang.strftime(record.timesheet_line.date)
                raise UserError(gettext(
                    'labor_yield_timesheet.'
                    'msg_labor_yield_allocation_timesheet_exists',
                    employee=record.employee.rec_name,
                    date=_date))
        super(YieldAllocation, cls).delete(records)
