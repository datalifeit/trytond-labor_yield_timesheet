# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from itertools import groupby
from decimal import Decimal
import datetime
from trytond.model import ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import PYSONEncoder
from trytond.transaction import Transaction
from trytond.wizard import Button, StateView, Wizard, StateAction
from trytond.wizard import StateTransition
from trytond.exceptions import UserError
from trytond.i18n import gettext


class TimesheetLine(metaclass=PoolMeta):
    __name__ = 'timesheet.line'

    labor_yield_allocs = fields.One2Many('labor.yield.allocation',
        'timesheet_line', 'Labor yield allocations')


class CreateLaborYieldTimesheet(Wizard):
    """Create labor yield timesheet line"""
    __name__ = 'labor.yield.create_timesheet'

    start = StateView('labor.yield.create_timesheet.start',
        'labor_yield_timesheet.create_timesheet_start_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('OK', 'create_', 'tryton-ok', default=True)])
    create_ = StateAction('timesheet.act_line_form')

    def default_start(self, fields):
        pool = Pool()
        Work = pool.get('timesheet.work')

        works = Work.search([
            ('yield_available', '=', True)])
        if len(works) == 1:
            return {'work': works[0].id}
        return {}

    def do_create_(self, action):
        pool = Pool()
        YieldAlloc = pool.get('labor.yield.allocation')
        Timesheet = pool.get('timesheet.line')
        Uom = pool.get('product.uom')

        _yield_allocs = YieldAlloc.search([
            ('date', '>=', self.start.start_date),
            ('date', '<=', self.start.end_date),
            ('work', '=', self.start.work.id),
            ('employee', '!=', None)],
            order=[('date', 'ASC'), ('employee', 'ASC')])

        timesheets = []
        # Remove existing timesheet lines
        Timesheet.delete(list(set(
            alloc.timesheet_line for alloc in _yield_allocs if alloc.timesheet_line)))

        _prices = {}
        for key, grouped_allocs in groupby(_yield_allocs,
                key=lambda x: (x.date, x.employee.id)):
            grouped_allocs = list(grouped_allocs)
            date, employee_id = key
            _prices.setdefault(date,
                self.start.work.compute_unit_price(date))
            if not _prices[date][0]:
                raise UserError(gettext(
                    'labor_yield_timesheet.'
                    'msg_labor_yield_create_timesheet_missing_unit_price',
                    work=self.start.work.rec_name,
                    date=self._format_date(date)))

            timesheet = {
                'company': grouped_allocs[0].employee.company.id,
                'employee': employee_id,
                'date': date,
                'work': self.start.work.id,
                'quantity': 0,
                'labor_yield_allocs': [('add', [])]}
            for alloc in grouped_allocs:
                timesheet['labor_yield_allocs'][0][1].append(alloc.id)
                timesheet['quantity'] += Uom.compute_qty(
                    alloc.quantity_uom, alloc.quantity, _prices[date][1])
            amount = Decimal(timesheet['quantity']) * _prices[date][0]
            timesheet['duration'] = datetime.timedelta(
                hours=self._convert_yield_amount_to_duration(
                    grouped_allocs[0].employee, date, amount))
            _ = timesheet.pop('quantity')

            timesheets.append(timesheet)

        self._check_employee_dates(timesheets)
        if timesheets:
            timesheets = Timesheet.create(timesheets)

        action['pyson_domain'] = PYSONEncoder().encode(
            [('id', 'in', list(map(int, timesheets)))])
        return action, {}

    @classmethod
    def _check_employee_dates(cls, timesheets):
        pool = Pool()
        Employee = pool.get('company.employee')

        def by_employee(record):
            return record['employee']
        employee_dates = {k: [x['date'] for x in g] for k, g in groupby(
            sorted(timesheets, key=by_employee), by_employee)}
        employees = Employee.browse(employee_dates.keys())

        error_employees = []
        for e in employees:
            min_date = min(employee_dates[e.id])
            max_date = max(employee_dates[e.id])
            if (e.start_date and e.start_date > max_date) or \
                (e.end_date and e.end_date < min_date):
                error_employees.append(e.rec_name)
        if error_employees:
            raise UserError(gettext(
                    'labor_yield_timesheet.'
                    'msg_labor_yield_create_timesheet_employee_dates',
                    employess='\n'.join(error_employees)))

    @classmethod
    def _convert_yield_amount_to_duration(cls, employee, date, amount):
        try:
            _price = employee.compute_payroll_price(date)
        except AttributeError:
            _price = employee.compute_cost_price(date)
        if not _price:
            raise UserError(gettext(
                    'labor_yield_timesheet.'
                    'msg_labor_yield_create_timesheet_missing_cost_prices',
                    employee=employee.rec_name,
                    date=cls._format_date(date)))
        return float(amount / _price)

    @classmethod
    def _format_date(cls, date):
        Lang = Pool().get('ir.lang')
        lang = Lang.get()
        language = Transaction().language
        languages = Lang.search([('code', '=', language)])
        if not languages:
            languages = Lang.search([('code', '=', 'en_US')])
        language, = languages
        return lang.strftime(date)

    def transition_create_(self):
        return 'end'


class LaborYieldTimesheetStartMixin(object):

    work = fields.Many2One('timesheet.work', 'Work', required=True,
        domain=[('yield_available', '=', True)])
    start_date = fields.Date('Start date', required=True)
    end_date = fields.Date('End date', required=True)


class CreateLaborYieldTimesheetStart(ModelView, LaborYieldTimesheetStartMixin):
    """Start creating labor yield timesheet line"""
    __name__ = 'labor.yield.create_timesheet.start'


class DeleteLaborYieldTimesheet(Wizard):
    """Delete labor yield timesheet line"""
    __name__ = 'labor.yield.delete_timesheet'

    start = StateView('labor.yield.delete_timesheet.start',
        'labor_yield_timesheet.delete_timesheet_start_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('OK', 'delete_', 'tryton-ok', default=True)])
    delete_ = StateTransition()

    def default_start(self, fields):
        pool = Pool()
        Work = pool.get('timesheet.work')

        works = Work.search([
            ('yield_available', '=', True)])
        if len(works) == 1:
            return {'work': works[0].id}
        return {}

    def transition_delete_(self):
        pool = Pool()
        YieldAlloc = pool.get('labor.yield.allocation')
        Timesheet = pool.get('timesheet.line')

        _yield_allocs = YieldAlloc.search([
            ('date', '>=', self.start.start_date),
            ('date', '<=', self.start.end_date),
            ('work', '=', self.start.work.id),
            ('timesheet_line', '!=', None)],
            order=[('date', 'ASC'), ('employee', 'ASC')])

        # Remove existing timesheet lines
        Timesheet.delete(list(set(
            alloc.timesheet_line for alloc in _yield_allocs
                if alloc.timesheet_line)))

        return 'end'


class DeleteLaborYieldTimesheetStart(ModelView, LaborYieldTimesheetStartMixin):
    """Start deleting labor yield timesheet line"""
    __name__ = 'labor.yield.delete_timesheet.start'
